# Here is a simple scout map creation/manipulation tool based on open-sourced project [Image Editor]

# Map Description
Black - "occupied" - corresponds to the areas not accessible
Red - Station Markers (a basic station separation is implemented, i.e., new stations cannot be marked if it is close to any of previous ones than 30 pixels)
White -  "free" - corresponds to the areas accessible or free to travel (in ovo, it should be more or less how our path is configured!)

# Data Storage
two numpy data files are created and stored:
* [saved_stationArr.npy](https://bitbucket.org/kevinNT2018/pat_map/src/master/saved_stationArr.npy), where a list/array stores the locations of all stations and the sequence should be aligned with traveling direction!
* [saved_mapArr.npy](https://bitbucket.org/kevinNT2018/pat_map/src/master/saved_mapArr.npy), where mapArr stores all the occupancy values in a 2d grid (RGB values: [0,0,0] - occupied, [255,0,0] - station, [255,255,255] - free)

# Operation Instructions
1. create your new maps
	- draw paths (use White color, hold leftclick on your mouse and move)
	- mark the stations (choose Red color, double-click on the locations you want but remember the station separation check, ~30pixels)
2. save your maps
3. [optional], you may open a pre-saved map from a pre-saved "saved_mapArr.npy" file

# How it can be used for two systems [ita vs ovo]
## ita
The closest vehicle can be found by computing the distance between all available vehicles to the target station [e.g., shortest path?]. Ideally, the map should be aligned with the laser scanned version or we create map from laser scan (some further integration work is pending!!!)

## ovo
The closest vehicle can be found by checking the last_seen_station_marker and compute the sequence distance. Since all stations are marked according to vehicle traveling direction, it is quite easy to find out which available vehicle is closest to the destination with the help of last_seen_station_marker and corresponding timestamps.

## talk to server
For each new trip-order, server sends out the pre-orders to all available vehicles and the vechile may return their distances-to-travel to the server. Then server may do simple distance sorting and assign the closest vechile to the trip-order!

1. All robots and terminals connect to the server
2. Passengers send out trip-orders to the server via either web-interface or terminals
3. Server sends out a trip-order distance check to all available robots (in specific pick-up regions, via last-seen stations or GPS locations)
4. Robots calculate their distance to the pick-up stations and check if their battery is sufficient for the trip (imagine what Uber drivers do to decide which orders to take?). And Robots sufficient for the trip reply to the server with their distance to the destination.
5. Server sorts the robot-serving-sequence by their distance to the pick-up destination and send out the final trip-order to the selected "closest" robot
6. The selected robot will implement the trip-order!