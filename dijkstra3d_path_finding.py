import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import dijkstra3d

presaved_scoutMap_file = "/home/kevin/lib4NT/Image-Editor/saved_mapArr.npy"
presaved_scoutStation_file = "/home/kevin/lib4NT/Image-Editor/saved_stationArr.npy"

test_gridRGBArr = np.load(presaved_scoutMap_file)
test_gridBinArr = test_gridRGBArr[:,:,0]+test_gridRGBArr[:,:,1]+test_gridRGBArr[:,:,2]
occupiedPixelYXCoordList = np.where(test_gridBinArr==0)
occupancyGridArr_binary = np.zeros([test_gridRGBArr.shape[0], test_gridRGBArr.shape[1]], dtype=np.int) 
# for y,x in occupiedPixelYXCoordList:
occupancyGridArr_binary[occupiedPixelYXCoordList[0],occupiedPixelYXCoordList[1]] = 1
print "occupiedPixelYXCoordList = ", occupiedPixelYXCoordList
print "len(occupiedPixelYXCoordList[0]) = ", len(occupiedPixelYXCoordList[0])
print "occupancyGridArr_binary = ", occupancyGridArr_binary
print "occupancyGridArr_binary.shape = ", occupancyGridArr_binary.shape

occupancyGrid3dArr_binary = occupancyGridArr_binary.reshape([occupancyGridArr_binary.shape[0], occupancyGridArr_binary.shape[1], 1])
print "occupancyGrid3dArr_binary.shape = ", occupancyGrid3dArr_binary.shape


stationsArr = np.load(presaved_scoutStation_file)
print "stationsArr = ", stationsArr

test_start = ("Station %d"%(0+1), (stationsArr[0][1], stationsArr[0][0])) # x,y
test_goal = ("Station %d"%(1+1), (stationsArr[1][1], stationsArr[1][0]))  # x,y

test_start_3d =  np.array([stationsArr[0][1], stationsArr[0][0],0]) # x,y
test_goal_3d = np.array([stationsArr[1][1], stationsArr[1][0],0]) # x,y

path = dijkstra3d.dijkstra(occupancyGrid3dArr_binary, test_start_3d, test_goal_3d)
print "path = ", path
print "path.shape = ", path.shape
print "len(path) = ", len(path)

DG = nx.DiGraph()

if stationsArr.shape[0]>1:
    for station_cnt in range(stationsArr.shape[0]):
        DG.add_node( ("Station %d"%(station_cnt+1), (stationsArr[station_cnt][1], stationsArr[station_cnt][0])) )
        if station_cnt>0:
            DG.add_weighted_edges_from( [(("Station %d"%(station_cnt), (stationsArr[station_cnt-1][1], stationsArr[station_cnt-1][0])), ("Station %d"%(station_cnt+1), (stationsArr[station_cnt][1], stationsArr[station_cnt][0])), 1.0)] )
            # DG.add_weighted_edges_from( [(station_cnt-1, station_cnt, 1.0)] )
        if station_cnt == stationsArr.shape[0]-1:
            DG.add_weighted_edges_from( [(("Station %d"%(station_cnt+1), (stationsArr[station_cnt][1], stationsArr[station_cnt][0])), ("Station %d"%(0+1), (stationsArr[0][1], stationsArr[0][0])), 1.0)] )
            # DG.add_weighted_edges_from( [(station_cnt, 0, 1.0)] )


# DG.add_node(1)
# DG.add_weighted_edges_from([(1, 2, 1.0), (2, 3, 1.0), (3, 1, 1.0)])
# DG.add_node(2)
# DG.add_node(3)
# DG.add_node(4)

print "DG = ", DG

print "shortest path = ", nx.shortest_path(DG, test_start, test_goal)
print "length of shortest path = ", nx.shortest_path_length(DG, test_start, test_goal)


nx.draw(DG, with_labels=True, font_weight='bold')

plt.show()
