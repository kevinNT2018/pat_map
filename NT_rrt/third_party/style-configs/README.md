# style-configs

This repo contains config files that specify how code should be formatted, allowing us to easily keep code formatting consistent in an automated way.

Contents:

* .clang-format - Config file for [clang-format](http://clang.llvm.org/docs/ClangFormat.html), which formats C/C++ code
* .style.yapf - Config file for [yapf](https://github.com/google/yapf), which formats Python code

**Note**: These config files are used by multiple RoboJackets teams, so please consult with each of them before making changes.
