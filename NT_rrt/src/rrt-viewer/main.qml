import QtQuick 2.5
import QtQuick.Window 2.1
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

import RRTWidget 1.0

ApplicationWindow {
    title: "Interactive RRT"

    // makes window floating by default in tiling window managers
    modality: Qt.WindowModal

    ColumnLayout {
        anchors.fill: parent;

        // main toolbar
        Row{
            // Add back when we use QtQuick 2.7+
            /* padding: 5 */
            /* spacing: 5 */
            Layout.alignment: Qt.AlignTop

            ColumnLayout {
                Button {
                    text: "Load"
                    onClicked: rrt.loadObstacles()
                }

                Button {
                    text: "Save"
                    onClicked: rrt.saveObstacles()
                }
            }

            ColumnLayout {
                Button {
                    onClicked: rrt.run()
                    text: "Run"
                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 1
                        color: "green"
                        opacity : .5
                    }
                }

                Button {
                    text: "Stop"
                    onClicked: rrt.stop()
                }
            }

            ColumnLayout {
                Button {
                    text: "Step"
                    onClicked: rrt.step()
                }

                Button {
                    text: "Step 100"
                    onClicked: rrt.stepBig()
                }
            }

            ColumnLayout {
                Button {
                    text: "SetStartEnd"
                    onClicked: rrt.setStartGoalState()
                }

                Button {
                    text: "SetStartEnd"
                    onClicked: rrt.setStartGoalState()
                }
            }

            ColumnLayout {
                Button {
                    text: "Reset"
                    onClicked: rrt.reset()
                }

                Button {
                    text: "Clear Obstacles"
                    onClicked: rrt.clearObstacles()

                    Rectangle {
                        anchors.fill: parent
                        anchors.margins: 1
                        color: "red"
                        opacity : .5
                    }
                }
            }

            ColumnLayout {
                Label {
                    text: "Goal Bias: " + goalBiasSlider.value.toFixed(2)
                }

                Slider {
                    id: goalBiasSlider
                    minimumValue: 0
                    maximumValue: 1.0
                    stepSize: 0.05
                    tickmarksEnabled: true
                    value: 0
                }
            }

            ColumnLayout {
                Label {
                    text: "Waypoint Bias: " + waypointBiasSlider.value.toFixed(2)
                }

                Slider {
                    id: waypointBiasSlider
                    stepSize: 0.05
                    minimumValue: 0
                    maximumValue: 1.0
                    tickmarksEnabled: true
                    value: 0
                }
            }

            ColumnLayout {
                Label {
                    text: "Step Size: "
                }

                SpinBox {
                    id: stepSizeBox
                    decimals: 1
                    value: 10
                }
            }

            ColumnLayout {
                Label {
                    text: "Adaptive\nStepsize"
                }

                CheckBox {
                    id: ascCheckbox
                    checked: false
                }
            }

            ColumnLayout {
                Label {
                    text: "startX: "
                }

                SpinBox {
                    id: startXBox
                    decimals: 1
                    value: 10
                    minimumValue: 0
                    maximumValue: 400.0
                }
            }

            ColumnLayout {
                Label {
                    text: "startY: "
                }

                SpinBox {
                    id: startYBox
                    decimals: 1
                    value: 10
                    minimumValue: 0
                    maximumValue: 400.0
                }
            }

            ColumnLayout {
                Label {
                    text: "endX: "
                }

                SpinBox {
                    id: endXBox
                    decimals: 1
                    value: 10
                    minimumValue: 0
                    maximumValue: 400.0
                }
            }

            ColumnLayout {
                Label {
                    text: "endY: "
                }

                SpinBox {
                    id: endYBox
                    decimals: 1
                    value: 10
                    minimumValue: 0
                    maximumValue: 400.0
                }
            }
        }

        // draw and interact with the rrt
        RRTWidget {
//            // Fixed size
//            Layout.maximumHeight: 600
//            Layout.maximumWidth: 1200
//            Layout.preferredHeight: 600
//            Layout.preferredWidth: 900
//            Layout.minimumHeight: 600
//            Layout.minimumWidth: 600
//            // Fixed size
//            Layout.maximumHeight: 800
//            Layout.maximumWidth: 800
//            Layout.preferredHeight: 800
//            Layout.preferredWidth: 800
//            Layout.minimumHeight: 800
//            Layout.minimumWidth: 800
            // Fixed size
            Layout.maximumHeight: 400
            Layout.maximumWidth: 400
            Layout.preferredHeight: 400
            Layout.preferredWidth: 400
            Layout.minimumHeight: 400
            Layout.minimumWidth: 400

            id: rrt
        }
    }

    // bottom bar
    statusBar: StatusBar {
        RowLayout {
            id: statusBarLayout
            anchors.fill: parent

            Label {
                text: "Iterations: " + rrt.iterations
                anchors.right: statusBarLayout.right
            }
        }
    }

    // update rrt values when the ui controls change
    Binding {
        target: rrt
        property: "goalBias"
        value: goalBiasSlider.value
    }
    Binding {
        target: rrt
        property: "waypointBias"
        value: waypointBiasSlider.value
    }
    Binding {
        target: rrt
        property: "stepSize"
        value: stepSizeBox.value
    }
    Binding {
        target: rrt
        property: "ascEnabled"
        value: ascCheckbox.checked
    }

    Binding {
        target: rrt
        property: "startX"
        value: startXBox.value
    }

    Binding {
        target: rrt
        property: "startY"
        value: startYBox.value
    }

    Binding {
        target: rrt
        property: "endX"
        value: endXBox.value
    }

    Binding {
        target: rrt
        property: "endY"
        value: endYBox.value
    }

    // keyboard shortcuts
    Shortcut { sequence: 'r'; onActivated: rrt.run() }
    Shortcut { sequence: 's'; onActivated: rrt.stop() }
    Shortcut { sequence: 'c'; onActivated: rrt.reset() }
    Shortcut{ sequence: 'o'; onActivated: rrt.clearObstacles() }
    Shortcut{ sequence: 't'; onActivated: rrt.step() }
    Shortcut{ sequence: 'b'; onActivated: rrt.stepBig() }

    Shortcut{ sequence: 'p'; onActivated: rrt.setStartGoalState() }
}
